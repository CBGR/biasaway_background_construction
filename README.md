Usage
-----

The script can be launched through the command:

    bash create_background_repository.sh -f /path/to/fastaFile -s kmer_size -r /path/to/outputDir

Description of the paramaters can be obtained by typing
`bash create_background_repository.sh -h`

Dependencies:
-------------

Two python libraries are required:

-   [BiasAway](https://bitbucket.org/CBGR/biasaway/src/master/)
-   [Pyfasta](https://github.com/brentp/pyfasta)

You can install them through `bioconda` or `pip` by typing for instance:

-   `pip install biasaway`
-   `pip install pyfasta`

Input:
-----

The only input file needed to create a background repository with BiasAway is a genome fasta file. Below we provide the links to the genome fasta files used to create the background repositories in [BiasAway](https://biasaway.uio.no/):

-    [Homo sapiens: GRCh38/hg38](https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/)
-    [Mus musculus: mm10](https://hgdownload.soe.ucsc.edu/goldenPath/mm10/bigZips/)
-    [Rattus norvegicus: Rnor 6.0](ftp://ftp.ensembl.org/pub/release-100/fasta/rattus_norvegicus/dna/)
-    [Arabidopsis thaliana: TAIR10](ftp://ftp.ensemblgenomes.org/pub/plants/release-47/fasta/arabidopsis_thaliana/dna/)
-    [Danio rerio: GRCz11](https://hgdownload.soe.ucsc.edu/goldenPath/danRer11/bigZips/)
-    [Drosophila melanogaster: dm6](https://hgdownload.soe.ucsc.edu/goldenPath/dm6/bigZips/)
-    [Caenorhabditis elegans: WBcel235](ftp://ftp.ensembl.org/pub/release-100/fasta/caenorhabditis_elegans/dna/)
-    [Saccharomyces cerevisiae](ftp://ftp.ensembl.org/pub/release-100/fasta/saccharomyces_cerevisiae/dna/)
-    [Schizosaccharomyces pombe: ASM294v2](ftp://ftp.ensemblgenomes.org/pub/fungi/release-47/fasta/schizosaccharomyces_pombe/dna/)

Please note that some genome fasta files are separated by chromosomes in their original repositories. In that case, please make sure to concatenate all chromosome fasta files in one single genome fasta file.

Output:
------

The output directory will contain the necessary files to run the
[BiasAway](https://bitbucket.org/CBGR/biasaway/src/master/) tool for modules
`g` and `c`.

Precomputed files:
------------------

We provide a collection of precomputed background repositories for the nine organisms mentioned above using k-mers of size 100, 250, 500, 750 and 1000 base pairs. They can be found as individual compressed files in our [Zenodo repository](https://zenodo.org/record/3923866).